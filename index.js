// <!-- //Get Post Data -->

// fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data))

fetch('https://jsonplaceholder.typicode.com/posts').then((response) => {
	return response.json()
})
.then((data) => {

	return showPosts(data)
})

//Syntax: fetch(url, options)

//Add Post

document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {

		method: 'POST',
		body: JSON.stringify({
			title:document.querySelector('#txt-title').value,
			body:document.querySelector('#txt-body').value,
			userId: 1

		}),

		headers: {
			 'Content-Type' : 'application/json'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('Post successfully added')
//null to reset input fields
		document.querySelector('#txt-title').value = null

		document.querySelector('#txt-body').value = null
	})
})

//Show Post

const showPosts = (posts) => {
	let postEntries = ''


	posts.forEach((post) => {
		postEntries += `
		<div id ="post-${post.id}">
		    <h2 id = "post-title-${post.id}">${post.title}</h2>
		     <p id = "post-body-${post.id}">${post.body}</p>

		     <button onClick = "editPost('${post.id}')">Edit</button>

		     <button onClick = "deletePost('${post.id}')">Delete</button>
		     </div>
	    `;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

//Edit Post

const editPost = (id) => {
	//title and body from inner html
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

   // get input values
	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body
// to enable update button when editing
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

//Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {

		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1

		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})

	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('Post successfully edited')

		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)


	})

})

//DELETE
const deletePost = (deleteId) => {


fetch(`https://jsonplaceholder.typicode.com/posts/${deleteId}`, 
 {
		method: 'DELETE'
	})

	document.querySelector(`#post-${deleteId}`).remove();
	
		console.log(data)
		
	
}


		




	

